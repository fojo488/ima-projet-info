#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "jeu.h"

char nom_joueur1[100];
char nom_joueur2[100];

int jcj(int championnat){
int flag_fin=0;
int *tab;
char bot='£';
int com=1;
if (championnat==0){
printf("\t Nom du joueur 1 :");
scanf("%s", nom_joueur1);
printf("\t Nom du joueur 2 :");
scanf("%s", nom_joueur2);
}
do{com=commence(nom_joueur1,nom_joueur2);}
while(com != 1 && com !=2);

afficher_jeu(0); // 0 => génération de la table de jeu 1 => update de la table de jeux
while (flag_fin==0){
    coup_jouer(1,bot,nom_joueur1);
    init_tab_verif();
    //afficher_tab_verif();
    algorith_de_jeu(0,9,1);
    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}
    coup_jouer(2,bot,nom_joueur2);
    algorith_de_jeu(9,0,2);
    //afficher_tab_verif();
    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}
    //algorith_de_jeu(0,9);
    }
return flag_fin;
}




int joueur_1_random(int championnat){

int nb_alea;
int flag_fin=0;
int *tab;
int com;

if (championnat==0){
printf("\t Nom du joueur 1 :");
scanf("%s", nom_joueur1);
printf("\t Nom du joueur 2 :");
scanf("%s", nom_joueur2);
}

do{com=commence(nom_joueur1,nom_joueur2);}
while(com != 1 && com !=2);

afficher_jeu(0); // 0 => génération de la table de jeu 1 => update de la table de jeux
while (flag_fin==0){
    srand(time(NULL));
    nb_alea = (rand() % 7) + 65;
    init_tab_verif();
    if (com==1){
    coup_jouer(1,nb_alea,nom_joueur1);
    algorith_de_jeu(0,9,1);
    }
    else {coup_jouer(2,nb_alea,nom_joueur2);
    algorith_de_jeu(9,0,2);}

    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}

    init_tab_verif();
    if (com==1){
    coup_jouer(2,nb_alea,nom_joueur2);
    algorith_de_jeu(9,0,2);
    }
    else {coup_jouer(1,nb_alea,nom_joueur1);
    algorith_de_jeu(0,9,1);}

    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}
    }
return flag_fin;
}


int joueur_contre_bot(int numero_bot,int championnat){


int coup;
int flag_fin=0;
int *tab;
int com;

if (championnat==0){
printf("\t Nom du joueur 1 :");
scanf("%s", nom_joueur1);
printf("\t Nom du joueur 2 :");
scanf("%s", nom_joueur2);
}

do{com=commence(nom_joueur1,nom_joueur2);}
while(com != 1 && com != 2);

afficher_jeu(0); // 0 => génération de la table de jeu 1 => update de la table de jeux
while (flag_fin==0){
    if(numero_bot==122){
        coup=choix_coup_joueur_1_random_choisit_un_coup_jouable(2);
    }
    else if(numero_bot==123){
        coup=coup_glouton(2);
    }
    else if(numero_bot==124){
        coup=coup_glouton_prevoyant(2);
    }
    else if(numero_bot==125){
        coup=coup_hegemonique(2);
    }

    init_tab_verif();
    if (com==1){
    coup_jouer(1,coup,nom_joueur1);
    algorith_de_jeu(0,9,1);
    }
    else {coup_jouer(2,coup,nom_joueur2);
    algorith_de_jeu(9,0,2);}

    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}

    init_tab_verif();
    if (com==1){
    coup_jouer(2,coup,nom_joueur2);
    algorith_de_jeu(9,0,2);
    }
    else {coup_jouer(1,coup,nom_joueur1);
    algorith_de_jeu(0,9,1);}

    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}


    }
return flag_fin;
}



int bot_contre_bot(int bot1,int bot2,int championnat){

int coup1,coup2;
int flag_fin=0;
int *tab;
int com;


if (championnat==0){
printf("\t Nom du joueur 1 :");
scanf("%s", nom_joueur1);
printf("\t Nom du joueur 2 :");
scanf("%s", nom_joueur2);
}

do{com=commence(nom_joueur1,nom_joueur2);}
while(com != 1 && com != 2);



afficher_jeu(0); // 0 => génération de la table de jeu 1 => update de la table de jeux
while (flag_fin==0){
    init_tab_verif();
    if (com==1){
    coup1=choix_bot_contre_bot(bot1,1);
    init_tab_verif();
    coup_jouer_bot_contre_bot(1,coup1,nom_joueur1);
    algorith_de_jeu(0,9,1);
    }
    else {
    coup2=choix_bot_contre_bot(bot2,2);
    init_tab_verif();
    coup_jouer_bot_contre_bot(2,coup2,nom_joueur2);
    algorith_de_jeu(9,0,2);}

    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}

    init_tab_verif();
    if (com==1){
    coup2=choix_bot_contre_bot(bot2,2);
    init_tab_verif();
    coup_jouer_bot_contre_bot(2,coup2,nom_joueur2);
    algorith_de_jeu(9,0,2);
    }

    else {
    coup1=choix_bot_contre_bot(bot1,1);
    init_tab_verif();
    coup_jouer_bot_contre_bot(1,coup1,nom_joueur1);
    algorith_de_jeu(0,9,1);}

    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}


    }
return flag_fin;
}

int *choix_bot(){

static int bot[2];

printf("Choisis un premier Bot : \n");
printf("\t 131 - Bot : Hasard prevoyant  \n");
printf("\t 132 - Bot : Glouton  \n");
printf("\t 133 - Bot : Glouton prevoyant   \n");
printf("\t 134 - Bot : Hegemonique   \n");
scanf("%d",&bot[0]);
printf("Choisis un deuxieme Bot : \n");
printf("\t 131 - Bot : Hasard prevoyant  \n");
printf("\t 132 - Bot : Glouton  \n");
printf("\t 133 - Bot : Glouton prevoyant   \n");
printf("\t 134 - Bot : Hegemonique   \n");
scanf("%d",&bot[1]);

return bot;

}

void nom_championnat(char nom1[10],char nom2[10]){
strcpy(nom_joueur1,nom1);
strcpy(nom_joueur2,nom2);
}



int championnat(){

debut_championnat();


return 0;
}





int bot_contre_bot_test(int championnat){

int coup1,coup2;
int flag_fin=0;
int *tab;
int com;


if (championnat==0){
printf("\t Nom du joueur 1 :");
scanf("%s", nom_joueur1);
printf("\t Nom du joueur 2 :");
scanf("%s", nom_joueur2);
}

do{com=commence(nom_joueur1,nom_joueur2);}
while(com != 1 && com != 2);



afficher_jeu(0); // 0 => génération de la table de jeu 1 => update de la table de jeux
while (flag_fin==0){
    init_tab_verif();
    if (com==1){
    coup1=coup_hegemonique(1);
    init_tab_verif();
    coup_jouer_bot_contre_bot(1,coup1,nom_joueur1);
    algorith_de_jeu(0,9,1);
    }
    else {
    coup2=choix_coup_joueur_1_random_choisit_un_coup_jouable(2);
    init_tab_verif();
    coup_jouer_bot_contre_bot(2,coup2,nom_joueur2);
    algorith_de_jeu(9,0,2);}

    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}

    init_tab_verif();
    if (com==1){
    coup2=choix_coup_joueur_1_random_choisit_un_coup_jouable(2);
    init_tab_verif();
    coup_jouer_bot_contre_bot(2,coup2,nom_joueur2);
    algorith_de_jeu(9,0,2);
    }

    else {
    coup1=coup_hegemonique(1);
    init_tab_verif();
    coup_jouer_bot_contre_bot(1,coup1,nom_joueur1);
    algorith_de_jeu(0,9,1);}

    afficher_jeu(1);
    tab=affichage_taux_occupation(nom_joueur1,nom_joueur2);
    flag_fin=gagnant_fin_de_partie(tab,nom_joueur1,nom_joueur2);
    if (flag_fin==1 || flag_fin==2){break;}


    }
return flag_fin;
}

