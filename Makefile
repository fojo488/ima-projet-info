prog : main.o option.o jeu.o
    gcc -o prog main.o option.o jeu.o

jeu.o: jeu.c
    gcc -o jeu.o -c jeu.c -Wall -Wall 

option.o: option.c jeu.h
    gcc -o option.o -c option.c -W -Wall

main.o: main.c option.h
    gcc -o main.o -c main.c -W -Wall